package com.example.app3.adapter

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.app3.R
import com.example.app3.model.OilPrice

class ItemAdapter (private val context: Context, private val dataset: List<OilPrice>) :
    RecyclerView.Adapter<ItemAdapter.ItemViewHolder>() {

    ////// customize
    class ItemViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        val textName: TextView = view.findViewById(R.id.item_name)
        val textPrice: TextView = view.findViewById(R.id.item_price)
    }
    //////

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val adapterLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item, parent, false)
        return ItemViewHolder(adapterLayout)
    }

    ////// customize
    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = dataset[position]
        holder.textName.text = context.resources.getString(item.stringName)
        holder.textPrice.text = context.resources.getString(item.stringPrice)


        holder.itemView.setOnClickListener(View.OnClickListener {
            Toast.makeText(
                holder.itemView.getContext(),
                "ผลิตภัณฑ์ : " + holder.textName.text + " ราคา : " + holder.textPrice.text + " บาท/ลิตร",
                Toast.LENGTH_LONG
            ).show()
        })


    }
    /////

    override fun getItemCount(): Int {
        return dataset.size
    }


}



