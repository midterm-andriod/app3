package com.example.app3.data

import com.example.app3.R
import com.example.app3.model.OilPrice

class Datasource {
    fun loadOilPrice() : List<OilPrice>{
        return listOf<OilPrice>(
            OilPrice(R.string.oil_name1,R.string.oil_price1),
            OilPrice(R.string.oil_name2,R.string.oil_price2),
            OilPrice(R.string.oil_name3,R.string.oil_price3),
            OilPrice(R.string.oil_name4,R.string.oil_price4),
            OilPrice(R.string.oil_name5,R.string.oil_price5),
            OilPrice(R.string.oil_name6,R.string.oil_price6),
            OilPrice(R.string.oil_name7,R.string.oil_price7),
            OilPrice(R.string.oil_name8,R.string.oil_price8),
            OilPrice(R.string.oil_name9,R.string.oil_price9),
        )
    }
}