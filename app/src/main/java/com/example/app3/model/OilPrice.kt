package com.example.app3.model

import androidx.annotation.StringRes

data class OilPrice(@StringRes val stringName: Int, @StringRes val stringPrice: Int)